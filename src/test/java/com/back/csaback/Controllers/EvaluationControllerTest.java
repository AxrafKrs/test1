package com.back.csaback.Controllers;

import com.back.csaback.Models.Evaluation;
import com.back.csaback.DTO.EvaluationDetails;
import com.back.csaback.Services.EvaluationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EvaluationController.class)
public class EvaluationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EvaluationService evaluationService;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(roles = "ADM")
    public void getAllEvaluationsTest() throws Exception {
        List<Evaluation> evaluations = new ArrayList<>();
        given(evaluationService.getAll()).willReturn(evaluations);

        mockMvc.perform(get("/eva/getAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(evaluations)));
    }

    @Test
    @WithMockUser(roles = "ADM")
    public void consulterInfoTest() throws Exception {
        Integer id = 1;
        EvaluationDetails details = new EvaluationDetails();
        given(evaluationService.findById(id)).willReturn(new Evaluation());
        given(evaluationService.ConsulterEvaluation(evaluationService.findById(id))).willReturn(details);
        mockMvc.perform(get("/eva/consulterInfo/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
